import { Component } from '@angular/core';
import { ListActorsComponent } from './list-actors/list-actors.component';
import { freeApiService } from './service/freeapi.service';
import { Actores } from './class/actores';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'prueba-front';

}
