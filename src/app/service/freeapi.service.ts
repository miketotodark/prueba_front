import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParameterCodec } from '@angular/common/http';

@Injectable()
export class freeApiService {

  constructor(private httpclient: HttpClient) {}

  getActors(): Observable<any> {

    //return this.httpclient.get('https://jsonplaceholder.typicode.com/comments');
    return this.httpclient.get('http://127.0.0.1:8000/service/get_actors/');
  }

}
