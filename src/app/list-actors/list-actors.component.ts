import { Component, OnInit } from '@angular/core';
import { Actores } from '../class/actores';
import { freeApiService } from '../service/freeapi.service';

@Component({
  selector: 'app-list-actors',
  templateUrl: './list-actors.component.html',
  styleUrls: ['./list-actors.component.css']
})
export class ListActorsComponent implements OnInit {

  constructor(private req_freeapiservice: freeApiService) {

  }

  listActors: Actores[];

  ngOnInit() {

    this.req_freeapiservice.getActors()
    .subscribe
    (
      data =>
      {
        this.listActors = data.data;
      }
    );

  }

}
