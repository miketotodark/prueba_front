import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListActorsComponent } from './list-actors/list-actors.component';
import { freeApiService } from './service/freeapi.service';

@NgModule({
  declarations: [
    AppComponent,
    ListActorsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [freeApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
